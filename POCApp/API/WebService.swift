//
//  WebService.swift
//  POCApp
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
import UIKit


final class WebService {
    
    
    func fetchData(api: String, completion :@escaping ((Result<Data>) -> Void)) {
        
        let url: URL = URL(string: api)!
        var request = URLRequest(url: url)
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Accept")
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {
                completion(.failure(WebServiceError.noData))
                return
            }
            if let error = error {
                completion(.failure(error))
                return
            }
            let str = String(data: data, encoding: .isoLatin1)
            
            print(str)
            
            let data1 = str!.data(using: .utf8)!
            print(data1)
            
            let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
            print(json)
            
            completion(.success(data1))
        }
        task.resume()
        
    }
    
    
}

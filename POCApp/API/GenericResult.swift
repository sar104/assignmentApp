//
//  GenericResult.swift
//  POCApp
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

enum Result<T> {
    
    case success(T)
    case failure(Error)
}

enum WebServiceError: Error {
    
    case noData
}

//
//  ResponseData.swift
//  POCApp
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

struct ResultData: Decodable {
    
    var title: String
    var rows: [ResponseData]?
    
    enum CodingKeys: String, CodingKey {
        
        case title
        case rows
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        rows = try container.decodeIfPresent([ResponseData].self, forKey: .rows)
    }
    
}
struct ResponseData {
    
    let title : String?
    let description : String?
    let imageHref : String?
    
    enum CodingKeys: String, CodingKey {
        
        case title
        case description
        case imageHref
    }
}

extension ResponseData: Decodable {
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        title = try container.decodeIfPresent(String.self, forKey: .title)
        description = try container.decodeIfPresent(String.self, forKey: .description)
        imageHref = try container.decodeIfPresent(String.self, forKey: .imageHref)
    }
}


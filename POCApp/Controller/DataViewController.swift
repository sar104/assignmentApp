//
//  DataViewController.swift
//  POCApp
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

    var dataArr: [DataViewModel] = []
    let dataTableView: UITableView = UITableView()
    
    let serviceManager = WebServiceManager(service: WebService())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNetworkStatus), name: NSNotification.Name(rawValue: NetworkStatusChanged), object: nil)
        
        setupUI()
        callAPI()
    }
    
    @objc func handleNetworkStatus() {
        
        self.alert(strTitle: alertTitle, strMsg: networkErrMSg)
    }
    
    func callAPI() {
        
        if Connectivity.isConnectedToInternet(){
            
            serviceManager.getData { (result) in
                switch result {
                    
                case .success(let data):
                    print("success:\(data)")
                    if let resultData = data as? ResultData {
                        if let rows = resultData.rows {
                            for val in rows {
                                
                                self.dataArr.append(DataViewModel(data: val))
                            }
                        }
                        DispatchQueue.main.async {
                            self.setNavigation(title: resultData.title)
                        }
                    }
                    DispatchQueue.main.async {
                        
                        self.dataTableView.reloadData()
                    }
                case .failure(let error):
                    print("error:\(error)")
                    self.alert(strTitle: alertTitle, strMsg: error.localizedDescription)
                }
            }
        } else {
            self.alert(strTitle: alertTitle, strMsg: networkErrMSg)
        }
    }
    
    func setupUI(){
        
        view.addSubview(dataTableView)
        
        dataTableView.translatesAutoresizingMaskIntoConstraints = false
        dataTableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        dataTableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        dataTableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        dataTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        dataTableView.rowHeight = UITableView.automaticDimension
        dataTableView.estimatedRowHeight = 100
        
        dataTableView.dataSource = self
        dataTableView.separatorStyle = .none
        dataTableView.register(DataCell.self, forCellReuseIdentifier: "cell")
    }
    
    func setNavigation(title: String){
        
        navigationItem.title = title
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DataViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DataCell
        cell.dataViewModel = dataArr[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.selectionStyle = .none
    }
}

//
//  DataCell.swift
//  POCApp
//
//  Created by Apple on 1/27/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class DataCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        
        self.setLayoutConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        //set the values for top,left,bottom,right margins
        let margins = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        
        contentView.frame.inset(by: margins)
    }
    
    private func setLayoutConstraints() {
        
        let marginGuide = contentView.layoutMarginsGuide
        
        self.contentView.addSubview(imgView)
        imgView.translatesAutoresizingMaskIntoConstraints = false
        // imageview constraints
        imgView.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 20).isActive = true
        imgView.bottomAnchor.constraint(greaterThanOrEqualTo: marginGuide.bottomAnchor).isActive = true
        imgView.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor, constant: 10).isActive = true
        imgView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        imgView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        // title label constraints
        self.contentView.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor, constant: 20).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: imgView.trailingAnchor, constant: 10).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: 10).isActive = true
        //titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 40)
        titleLabel.numberOfLines = 0
        
        // description label constraints
        
        self.contentView.addSubview(descrLabel)
        descrLabel.translatesAutoresizingMaskIntoConstraints = false
        descrLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
        descrLabel.leadingAnchor.constraint(equalTo: imgView.trailingAnchor, constant: 10).isActive = true
        descrLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: 10).isActive = true
        descrLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor, constant: 25).isActive = true
        descrLabel.numberOfLines = 0
    }
    
    let imgView:UIImageView = {
       
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        
        
        return img
    }()
    
    let titleLabel: UILabel = {
       
        let label = UILabel()
        
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.textColor = UIColor.black
        
        return label
    }()
    
    let descrLabel: UILabel = {
        
        let label = UILabel()
        
        label.font = UIFont.systemFont(ofSize: 15)
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.textColor = UIColor.black
        
        return label
    }()
    
    var dataViewModel: DataViewModel? {
        
        didSet {
            guard let data = dataViewModel else {return}
            
            titleLabel.text = data.getTitle()
            descrLabel.text = data.getDescr()
            let url = URL(string: data.getImg())
            imgView.sd_setImage(with: url) { (image, error, imgCacheType, url) in
                
            }
            
        }
    }
  
}
